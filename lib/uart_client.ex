defmodule UartClient do
  @moduledoc """
  Documentation for UartClient.
  """

  @doc """
  Hello world.

  ## Examples

      iex> UartClient.hello()
      :world

  """
  def hello do
    :world
  end
end
